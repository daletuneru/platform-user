import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { overwriteRoute, WinofficeLibModule } from 'winoffice-lib';
import { UserAboutComponent } from './user-about/user-about.component';

overwriteRoute({
    path: 'about',
    component: UserAboutComponent,
})

@NgModule({
    declarations: [
        AppComponent,
        UserAboutComponent,
    ],
    imports: [
        WinofficeLibModule,
        BrowserModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
